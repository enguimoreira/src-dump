hbase shell

drop 'base:table'

disable 'base:table'

list_regions 'base:table'

get 'base:table', 'rowkey_value'

put 'base:table', 'rowkey_value',  'column:cell', 'column_cell_value'

scan 'base:table', { FILTER => "RowFilter(=,'regexstring:.*rowkey_value')" }

scan 'base:table', { COLUMNS => 'column:cell', FILTER => "RowFilter(=,'regexstring:.*rowkey_value')", LIMIT => 5 }

scan 'base:table', { COLUMNS => 'column:cell', FILTER => "ValueFilter(=, 'substring:column_cell_value')", LIMIT => 5 }

scan 'base:table', { FILTER => "RowFilter(=,'regexstring:.*rowkey_value')", FILTER => "ColumnValueFilter('column', 'cell', =, 'binary:column_cell_value')", TIMERANGE => [0000000000000, 9999999999999], LIMIT => 5 }


create 'base:table', {
    NAME => 'column',
    DATA_CLOCK_ENCODING => 'FAST_DIFF',
    BLOOMFILTER => 'ROW',
    VERSIONS => '1',
    COMPRESSION => 'SNAPPY',
    KEEP_DELETED_CELLS => 'FALSE',
    BLOCKSIZE => '655336',
    IN_MEMORY => 'FALSE',
    BLOCKCACHE => 'TRUE'},
SPLITS => ["001","...", "999"]


create 'controle', 'produto', 'fornecedor'
create 'controle', {NAME='produto'},{NAME=>'fornecedor'}

put 'controle', '1', 'produto:nome', 'ram'
put 'controle', '1', 'produto:qtd', '100'

put 'controle', '2', 'produto:nome', 'hd'
put 'controle', '2', 'produto:qtd', '50'

put 'controle', '3', 'produto:nome', 'mouse'
put 'controle', '3', 'produto:qtd', '150'


put 'controle', '1', 'fornecedor:nome', 'TI Comp'
put 'controle', '1', 'fornecedor:estado', 'SP'

put 'controle', '2', 'fornecedor:nome', 'Peças PC'
put 'controle', '2', 'fornecedor:estado', 'MG'

put 'controle', '3', 'fornecedor:nome', 'Inf Tec'
put 'controle', '3', 'fornecedor:estado', 'SP'

list
describe 'controle'
count 'controle'

scan 'controle', {LIMIT => 15}

alter 'controle', {NAME=>'produto', VERSIONS=>3}

put 'controle', '2', 'produto:qtd', '200'

get 'controle', '2', {COLUMNS=>['produto:qtd'], VERSIONS=>3}

scan 'controle', {COLUMNS=>'fornecedor:estado', FILTER => "ValueFilter(=, 'binary:SP')"}

deleteall 'controle', '1'
deleteall 'controle', '3'

delete 'controle', '2', 'fornecedor:estado'

scan 'controle'
