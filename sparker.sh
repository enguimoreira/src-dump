curl -O https://repo1.maven.org/maven2/com/twitter/parquet-hadoop-bundle/1.6.0/parquet-hadoop-bundle-1.6.0.jar
docker cp parquet-hadoop-bundle-1.6.0.jar spark:/opt/spark/jars


hdfs dfs -put /input/exercises-data/juros_selic/ /user/aluno/enguimoreira/data
hdfs dfs -ls /user/aluno/enguimoreira/data/

spark-shell

val jurosDF = spark.read.json("/user/aluno/enguimoreira/data/juros_selic/juros_selic.json")

jurosDF.printSchema()

jurosDF.show(5)
jurosDF.show(5,false)

jurosDF.count()

val jurosDF10 = jurosDF.where("valor > 10")

jurosDF10.write.saveAsTable("enguimoreira.tab_juros_selic")

val jurosHiveDF = spark.read.table("enguimoreira.tab_juros_selic")

jurosHiveDF.printSchema()

jurosHiveDF.show(5)

jurosHiveDF.write.option("path","/user/aluno/enguimoreira/data/save_juros").save("juros_selicParquet")
jurosHiveDF.write.save("/user/aluno/enguimoreira/data/save_juros")

hdfs dfs -ls /user/aluno/enguimoreira/data/save_juros

val jurosHDFS = spark.read.load("/user/aluno/enguimoreira/data/save_juros/")

jurosHDFS.printSchema()

jurosHDFS.show(5)


val alunosDF = spark.read.csv("/user/aluno/enguimoreira/data/escola/alunos.csv")

alunosDF.printSchema()
alunosDF.show(5)

val alunosDF = spark.read.option("header","true").csv("/user/aluno/enguimoreira/data/escola/alunos.csv")

alunosDF.printSchema()
alunosDF.show(5)

val alunosDF = spark.read.option("inferSchema","true").option("header","true").csv("/user/aluno/enguimoreira/data/escola/alunos.csv")

alunosDF.printSchema()
alunosDF.show(5)

alunosDF.write.saveAsTable("enguimoreira.tab_alunos")

val cursosDF = spark.read.option("inferSchema","true").option("header","true").csv("/user/aluno/enguimoreira/data/escola/cursos.csv")

val alunos_cursosDF = alunosDF.join(cursosDF, "id_curso")
alunos_cursosDF.show(5)
alunos_cursosDF.printSchema()
alunos_cursosDF.count()


spark.catalog.listDatabases.show()

spark.catalog.setCurrentDatabase("enguimoreira")

spark.catalog.listTables.show()

spark.catalog.listColumns("tab_alunos").show

spark.sql("select * from tab_alunos").show(10)


spark.sql("select id_discente, nome from tab_alunos limit 5").show(false)
val alunosHiveDF = spark.read.table("tab_alunos")
alunosHiveDF.select("id_discente", "nome").limit(5).show(false)

spark.sql("select id_discente, nome, ano_ingresso from tab_alunos where ano_ingresso>= 2018").show(false)
alunosHiveDF.select("id_discente", "nome", "ano_ingresso").where("ano_ingresso>= 2018").show(false)

spark.sql("select id_discente, nome, ano_ingresso from tab_alunos where ano_ingresso>= 2018 order by nome desc").show(false)
alunosHiveDF.where("ano_ingresso>= 2018").orderBy($"nome".desc).show(false)
alunosHiveDF.where("ano_ingresso>= 2018").orderBy(desc("nome")).show(false)

spark.sql("select id_discente, nome, ano_ingresso from tab_alunos where ano_ingresso>= 2018 order by nome desc").count()
spark.sql("select count(*) from tab_alunos where ano_ingresso>= 2018").show(false)
alunosHiveDF.where("ano_ingresso>= 2018").count()




hdfs dfs -put /input/exercises-data/teste /user/aluno/enguimoreira/data/teste

val testeDF = spark.read.option("inferSchema","true").csv("/user/aluno/enguimoreira/data/teste")