hadoop fs -ls -d /hadoop/data/

hdfs dfs -get /hadoop/data/* /temp/

hdfs dfs -rm -R -skipTrash /hadoop/data/

yarn application -kill app_id

yarn logs -applicationId application_id -log_files stdout
