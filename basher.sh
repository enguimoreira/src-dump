pwd

cd etc

ls -lth /temp/

ls -1t -la | head -5

mkdir -p /mnt/ram

mkdir -p src/{1..100}/{1..100}

mkdir -p src/{1sub,2sub}/{1sub,2sub,3sub}

mkdir -t tmpfs tmpfs /mnt/ram -o size=8192M

dd if=/dev/zero of=ftest.iso bs=1M count=6000

chmod -R 700 src/*

chmod -x script.sh

ln env_conf.dev env.conf

scp z:\pth\file.log user@255.255.255.255:/path/

sh ./script.sh | tee $(date +%Y-%m-%d-%H%M%S)-script.log

less +F script.log

fc

sudo !!

#tunnel local port 3337 to remote on port 6379
ssh -L 3337:255.255.255.255:6379 user@data.org -N

reset

disown -a && exit

#ctrl+u - cut to the start

#ctrl+w - cut word

#ctrl+k - cut to the end

#ctrl+y - yank / recover

#ctrl+x+e - command editor

#alt+. - last successfull argument

# pwd - no history show leading space